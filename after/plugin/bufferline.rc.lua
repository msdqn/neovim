require'bufferline'.setup {}
vim.cmd[[
nnoremap <silent>bn :BufferLineCycleNext<CR>
nnoremap <silent>bp :BufferLineCyclePrev<CR>
]]
